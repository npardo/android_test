package com.genesys.genesystest.model


data class Image(var url: String,
                 var type: String,
                 var expression: String,
                 var width: String,
                 var height: String,
                 var description: String)


data class Article(var title: String,
                   var link: String,
                   var description: String,
                   var author: String,
                   var pubDate: String,
                   var category: List<String>,
                   var image: Image)
