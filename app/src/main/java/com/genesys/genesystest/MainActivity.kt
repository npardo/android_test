package com.genesys.genesystest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.genesys.genesystest.fetcher.FeedRequest
import com.genesys.genesystest.fetcher.Fetcher
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Request

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recycler = findViewById<RecyclerView>(R.id.recycler)
        recycler.layoutManager = LinearLayoutManager(this)
        Fetcher.fetch(FeedRequest(Request.Builder().url(
            "https://bold360ai-mobile-artifacts.s3.amazonaws.com/interview-json/feed.json"
                .toHttpUrlOrNull()
                ?.newBuilder()
                ?.build()
                .toString())
            .build())
        ) { result ->
            result.onSuccess {
                Log.d("Test", it.toString())
            }

        }
    }

    class FeedAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            TODO("Not yet implemented")
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            TODO("Not yet implemented")
        }

        override fun getItemCount(): Int {
            TODO("Not yet implemented")
        }

    }
}