package com.genesys.genesystest.fetcher

import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException


object Fetcher {
    fun <T : Any> fetch(request: GBaseRequest<T>, completion: (Result<T?>) -> (Unit)) {
        OkHttpClient().newCall(request.request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                completion(Result.failure(e))
            }

            override fun onResponse(call: Call, response: Response) {
                response.body?.bytes()?.let {
                    completion(Result.success(request.parser?.invoke(it)))
                }
            }
        })
    }
}