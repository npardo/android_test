package com.genesys.genesystest.fetcher

import com.genesys.genesystest.model.Article
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.Request

open class GBaseRequest<T>(var request: Request) {
    open val parser: ((ByteArray) -> T?)?
    get() = {
        null
    }
}

class FeedRequest(request: Request) : GBaseRequest<List<Article>>(request) {
    override val parser: ((ByteArray) -> List<Article>?)
        get() {
            return {
                val gson = Gson()
                val listType = object : TypeToken<List<Article>>() {}.type
                gson.fromJson<List<Article>>(String(it), listType)
            }
        }
}